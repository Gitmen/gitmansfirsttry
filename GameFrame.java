import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;

public class GameFrame extends Frame {
	
int i;
int j;


GameMapPanel mapPanel;
	Map map;
	Label label;
	Label la;
	TextField textField;
	TextArea textArea;
	Button button;
	
		
	GameFrame(Map m) {
		map = m;
		
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		
		
		
		
		textArea = new TextArea();
		textArea.setEditable(false);
		textArea.setLocation(350, 170);
		textArea.setSize(270, 150);
		textArea.setText("Enter numbers to complete the game.\n Only the first digit will be taken! \n Use w, a, s, d to move the + to the position you want.\n Numbers will be entered on the +'s location! \n If you guess right the + will change into the stated number\n");
		this.add(textArea);
			
			
		button = new Button();
		button.setLabel("Start");
		button.setLocation(50, 350);
		button.setSize(150, 30);
		GameActionListener gal = new GameActionListener(this);
		button.addActionListener(gal);
		this.add(button);
		
		mapPanel = new GameMapPanel(map);
        mapPanel.setLocation(50, 50);
		mapPanel.setSize(map.sizex * 30 + 1, map.sizey * 30 + 1);
	
		GameKeyListener gkl = new GameKeyListener(this);
		mapPanel.addKeyListener(gkl);
		GameMouseListener gml = new GameMouseListener(this);
		mapPanel.addMouseListener(gml);
		this.add(mapPanel);
		
		label = new Label();
		label.setLocation(350, 50);
		label.setSize(100, 30);

		this.add(label);
	
		
		this.setLayout(null);
	
	
		
	}
	
}
